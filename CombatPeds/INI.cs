﻿using GTA;
using NativeUI;
using System.Windows.Forms;

namespace SpawnCombatPeds
{
	static class INI
	{
		public static Keys SpawnKey, FightKey, EnterVehKey, ToggleMenuKey;
		public static bool RandomTargeting, RandomizeRange, RandomizeAbility, RandomizeAccuracy, SufferCriticalHits;
		public static int MaxCombatants, MaxDeadCombatants;
		public static float SeeingRange, EnterVeh_MaxDistancePlrToPed, EnterVeh_MaxDistancePedToVeh;

		public static bool CustomCombat, CanSwitchWeapons;
		public static bool UseFistWeapon, UseKnifeWeapons, UseMeleeWeapons, UsePistolWeapons,
			UseTaserWeapon, UseSMGWeapons, UseAssaultRifleWeapons, UseMGWeapons,
			UseShotgunWeapons, UseSniperWeapons, UseHeavyWeapons, UseSecondaryKnifeWeapon;

		internal static void GetSettings(ScriptSettings settings)
		{
			ToggleMenuKey = settings.GetValue("Keys", "ToggleMenu", Keys.F5);
			SpawnKey = settings.GetValue("Keys", "SpawnKey", Keys.J);
			FightKey = settings.GetValue("Keys", "FightKey", Keys.K);
			EnterVehKey = settings.GetValue("Keys", "EnterClosestVehicleKey", Keys.L);
			MaxCombatants = settings.GetValue("General", "MaxAliveCombatants", 40);
			MaxDeadCombatants = settings.GetValue("General", "MaxDeadCombatants", 30);
			EnterVeh_MaxDistancePlrToPed = settings.GetValue("General", "EnterVeh_MaxDistancePlayerToPed", 5f);
			EnterVeh_MaxDistancePedToVeh = settings.GetValue("General", "EnterVeh_MaxDistancePedToVehicle", 50f);
			CustomCombat = settings.GetValue("Combat", "Custom", false);
			CanSwitchWeapons = settings.GetValue("Combat", "CanSwitchWeapons", true);
			RandomTargeting = settings.GetValue("Combat", "RandomTargeting", true);
			RandomizeRange = settings.GetValue("Combat", "RandomRange", true);
			RandomizeAbility = settings.GetValue("Combat", "RandomAbility", true);
			RandomizeAccuracy = settings.GetValue("Combat", "RandomAccuracy", true);
			SeeingRange = settings.GetValue("Combat", "SeeingRange", 1000f);
			SufferCriticalHits = settings.GetValue("Combat", "SufferCriticalHits", false);
			UseFistWeapon = settings.GetValue("Weapons", "Fists", false);
			UseKnifeWeapons = settings.GetValue("Weapons", "Knives", false);
			UseMeleeWeapons = settings.GetValue("Weapons", "Melee", false);
			UsePistolWeapons = settings.GetValue("Weapons", "Pistols", true);
			UseTaserWeapon = settings.GetValue("Weapons", "Taser", false);
			UseSMGWeapons = settings.GetValue("Weapons", "SMGs", true);
			UseAssaultRifleWeapons = settings.GetValue("Weapons", "AssaultRifles", true);
			UseMGWeapons = settings.GetValue("Weapons", "MachineGuns", true);
			UseShotgunWeapons = settings.GetValue("Weapons", "Shotguns", true);
			UseSniperWeapons = settings.GetValue("Weapons", "Snipers", true);
			UseHeavyWeapons = settings.GetValue("Weapons", "Heavies", true);
			UseSecondaryKnifeWeapon = settings.GetValue("Weapons", "SecondaryKnife", false);

			if (Menu.cbCustomCombat != default(UIMenuCheckboxItem)) Menu.cbCustomCombat.Checked = CustomCombat;
			if (Menu.cbSwitchWeapons != default(UIMenuCheckboxItem)) Menu.cbSwitchWeapons.Checked = CanSwitchWeapons;
			if (Menu.cbFist != default(UIMenuCheckboxItem)) Menu.cbFist.Checked = UseFistWeapon;
			if (Menu.cbKnives != default(UIMenuCheckboxItem)) Menu.cbKnives.Checked = UseKnifeWeapons;
			if (Menu.cbMelee != default(UIMenuCheckboxItem)) Menu.cbMelee.Checked = UseMeleeWeapons;
			if (Menu.cbPistols != default(UIMenuCheckboxItem)) Menu.cbPistols.Checked = UsePistolWeapons;
			if (Menu.cbTaser != default(UIMenuCheckboxItem)) Menu.cbTaser.Checked = UseTaserWeapon;
			if (Menu.cbSMGs != default(UIMenuCheckboxItem)) Menu.cbSMGs.Checked = UseSMGWeapons;
			if (Menu.cbAssaults != default(UIMenuCheckboxItem)) Menu.cbAssaults.Checked = UseAssaultRifleWeapons;
			if (Menu.cbMGs != default(UIMenuCheckboxItem)) Menu.cbMGs.Checked = UseMGWeapons;
			if (Menu.cbShotguns != default(UIMenuCheckboxItem)) Menu.cbShotguns.Checked = UseShotgunWeapons;
			if (Menu.cbSnipers != default(UIMenuCheckboxItem)) Menu.cbSnipers.Checked = UseSniperWeapons;
			if (Menu.cbHeavies != default(UIMenuCheckboxItem)) Menu.cbHeavies.Checked = UseHeavyWeapons;
			if (Menu.cbKnife2nd != default(UIMenuCheckboxItem)) Menu.cbKnife2nd.Checked = UseSecondaryKnifeWeapon;
		}

		internal static void SaveSettings(ScriptSettings settings)
		{
			settings.SetValue("Keys", "ToggleMenu", ToggleMenuKey);
			settings.SetValue("Keys", "SpawnKey", SpawnKey);
			settings.SetValue("Keys", "FightKey", FightKey);
			settings.SetValue("Keys", "EnterClosestVehicleKey", EnterVehKey);
			settings.SetValue("General", "MaxAliveCombatants", MaxCombatants);
			settings.SetValue("General", "MaxDeadCombatants", MaxDeadCombatants);
			settings.SetValue("General", "EnterVeh_MaxDistancePlayerToPed", EnterVeh_MaxDistancePlrToPed);
			settings.SetValue("General", "EnterVeh_MaxDistancePedToVehicle", EnterVeh_MaxDistancePedToVeh);
			settings.SetValue("Combat", "Custom", CustomCombat);
			settings.SetValue("Combat", "CanSwitchWeapons", CanSwitchWeapons);
			settings.SetValue("Combat", "RandomTargeting", RandomTargeting);
			settings.SetValue("Combat", "RandomRange", RandomizeRange);
			settings.SetValue("Combat", "RandomAbility", RandomizeAbility);
			settings.SetValue("Combat", "RandomAccuracy", RandomizeAccuracy);
			settings.SetValue("Combat", "SeeingRange", SeeingRange);
			settings.SetValue("Combat", "SufferCriticalHits", SufferCriticalHits);
			settings.SetValue("Weapons", "Fists", UseFistWeapon);
			settings.SetValue("Weapons", "Knives", UseKnifeWeapons);
			settings.SetValue("Weapons", "Melee", UseMeleeWeapons);
			settings.SetValue("Weapons", "Pistols", UsePistolWeapons);
			settings.SetValue("Weapons", "Taser", UseTaserWeapon);
			settings.SetValue("Weapons", "SMGs", UseSMGWeapons);
			settings.SetValue("Weapons", "AssaultRifles", UseAssaultRifleWeapons);
			settings.SetValue("Weapons", "MachineGuns", UseMGWeapons);
			settings.SetValue("Weapons", "Shotguns", UseShotgunWeapons);
			settings.SetValue("Weapons", "Snipers", UseSniperWeapons);
			settings.SetValue("Weapons", "Heavies", UseHeavyWeapons);
			settings.SetValue("Weapons", "SecondaryKnife", UseSecondaryKnifeWeapon);

			settings.Save();
		}
	}
}

﻿using GTA;
using NativeUI;
using System;

namespace SpawnCombatPeds
{
	class Menu : Script
	{
		private static MenuPool menuPool;
		private static UIMenu menu, weaponsMenu;

		internal static UIMenuCheckboxItem cbCustomCombat, cbSwitchWeapons;
		internal static UIMenuItem miSave;
		internal static UIMenuCheckboxItem cbFist, cbKnives, cbMelee, cbPistols, cbTaser, cbSMGs,
			cbAssaults, cbMGs, cbShotguns, cbSnipers, cbHeavies, cbKnife2nd;

		public Menu()
		{
			menuPool = new MenuPool();

			menu = new UIMenu("SpawnCombatPeds", "Combatant settings");

			cbCustomCombat = new UIMenuCheckboxItem("Custom combat", INI.CustomCombat);
			cbSwitchWeapons = new UIMenuCheckboxItem("Can switch weapons", INI.CanSwitchWeapons);
			miSave = new UIMenuItem("Save settings");

			cbFist = new UIMenuCheckboxItem("Use fist", INI.UseFistWeapon);
			cbKnives = new UIMenuCheckboxItem("Use knives", INI.UseKnifeWeapons);
			cbMelee = new UIMenuCheckboxItem("Use melee", INI.UseMeleeWeapons);
			cbPistols = new UIMenuCheckboxItem("Use pistols", INI.UsePistolWeapons);
			cbTaser = new UIMenuCheckboxItem("Use taser", INI.UseTaserWeapon);
			cbSMGs = new UIMenuCheckboxItem("Use SMGs", INI.UseSMGWeapons);
			cbAssaults = new UIMenuCheckboxItem("Use assault rifles", INI.UseAssaultRifleWeapons);
			cbMGs = new UIMenuCheckboxItem("Use MGs", INI.UseMGWeapons);
			cbShotguns = new UIMenuCheckboxItem("Use shotguns", INI.UseShotgunWeapons);
			cbSnipers = new UIMenuCheckboxItem("Use snipers", INI.UseSniperWeapons);
			cbHeavies = new UIMenuCheckboxItem("Use heavies", INI.UseHeavyWeapons);
			cbKnife2nd = new UIMenuCheckboxItem("Give knife as secondary weapon", INI.UseSecondaryKnifeWeapon);

			cbCustomCombat.CheckboxEvent += CheckboxItems_CheckChanged;
			cbSwitchWeapons.CheckboxEvent += CheckboxItems_CheckChanged;
			miSave.Activated += MiSave_Activated;

			cbKnives.CheckboxEvent += CheckboxItems_CheckChanged;
			cbMelee.CheckboxEvent += CheckboxItems_CheckChanged;
			cbPistols.CheckboxEvent += CheckboxItems_CheckChanged;
			cbSMGs.CheckboxEvent += CheckboxItems_CheckChanged;
			cbAssaults.CheckboxEvent += CheckboxItems_CheckChanged;
			cbMGs.CheckboxEvent += CheckboxItems_CheckChanged;
			cbShotguns.CheckboxEvent += CheckboxItems_CheckChanged;
			cbSnipers.CheckboxEvent += CheckboxItems_CheckChanged;
			cbHeavies.CheckboxEvent += CheckboxItems_CheckChanged;
			cbKnife2nd.CheckboxEvent += CheckboxItems_CheckChanged;

			menu.AddItem(cbCustomCombat);
			menu.AddItem(cbSwitchWeapons);

			weaponsMenu = menuPool.AddSubMenu(menu, "Weapons");

			weaponsMenu.AddItem(cbKnives);
			weaponsMenu.AddItem(cbMelee);
			weaponsMenu.AddItem(cbPistols);
			weaponsMenu.AddItem(cbSMGs);
			weaponsMenu.AddItem(cbAssaults);
			weaponsMenu.AddItem(cbMGs);
			weaponsMenu.AddItem(cbShotguns);
			weaponsMenu.AddItem(cbSnipers);
			weaponsMenu.AddItem(cbHeavies);
			weaponsMenu.AddItem(cbKnife2nd);

			menuPool.Add(menu);

			Tick += Menu_Tick;
		}

		private void CheckboxItems_CheckChanged(UIMenuCheckboxItem sender, bool check)
		{
			if (sender == cbCustomCombat) INI.CustomCombat = check;
			else if (sender == cbSwitchWeapons) INI.CanSwitchWeapons = check;
			else if (sender == cbKnives) INI.UseKnifeWeapons = check;
			else if (sender == cbMelee) INI.UseMeleeWeapons = check;
			else if (sender == cbPistols) INI.UsePistolWeapons = check;
			else if (sender == cbSMGs) INI.UseSMGWeapons = check;
			else if (sender == cbAssaults) INI.UseAssaultRifleWeapons = check;
			else if (sender == cbMGs) INI.UseMGWeapons = check;
			else if (sender == cbShotguns) INI.UseShotgunWeapons = check;
			else if (sender == cbSnipers) INI.UseSniperWeapons = check;
			else if (sender == cbHeavies) INI.UseHeavyWeapons = check;
			else if (sender == cbKnife2nd) INI.UseSecondaryKnifeWeapon = check;
		}

		private void MiSave_Activated(UIMenu sender, UIMenuItem selectedItem)
		{
			INI.SaveSettings(Settings);
			UI.ShowSubtitle("Saved");
		}

		private void Menu_Tick(object sender, EventArgs e)
		{
			menuPool.ProcessMenus();
		}

		internal static void Toggle()
		{
			menu.Visible = !menu.Visible;
		}
	}
}

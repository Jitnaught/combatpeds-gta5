﻿using GTA;
using GTA.Math;

namespace SpawnCombatPeds
{
	class CombatPed
	{
		public Ped ped = null;
		public Model model = null;
		public Blip blip = null;
		public Vector3 lastPosWhileRunning = Vector3.Zero;
		public bool moveToNearestPed = false, getUnstuck = false, runningToUnstuck = false;
		public int timeFirstStuck = 0, timeStopUnstuck = 0;

		public CombatPed(Ped ped, Blip blip)
		{
			this.ped = ped;
			this.model = ped.Model;
			this.blip = blip;
		}
	}
}

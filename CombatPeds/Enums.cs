﻿using GTA.Native;
using System;
using System.Collections.Generic;
using System.IO;

namespace SpawnCombatPeds
{
	static class Enums
	{
		private static string pedModelsPath = "./scripts/SpawnCombatPeds/PedModels.txt",
			knifeWeaponsPath = "./scripts/SpawnCombatPeds/KnifeWeapons.txt",
			meleeWeaponsPath = "./scripts/SpawnCombatPeds/MeleeWeapons.txt",
			pistolWeaponsPath = "./scripts/SpawnCombatPeds/PistolWeapons.txt",
			smgWeaponsPath = "./scripts/SpawnCombatPeds/SMGWeapons.txt",
			arWeaponsPath = "./scripts/SpawnCombatPeds/ARWeapons.txt",
			mgWeaponsPath = "./scripts/SpawnCombatPeds/MGWeapons.txt",
			shotgunWeaponsPath = "./scripts/SpawnCombatPeds/ShotgunWeapons.txt",
			sniperWeaponsPath = "./scripts/SpawnCombatPeds/SniperWeapons.txt",
			heavyWeaponsPath = "./scripts/SpawnCombatPeds/HeavyWeapons.txt";

		public static PedHash[] PedModels = { };

		public static WeaponHash[] KnifeWeapons = { },
			MeleeWeapons = { },
			PistolWeapons = { },
			SMGWeapons = { },
			ARWeapons = { },
			MGWeapons = { },
			ShotgunWeapons = { },
			SniperWeapons = { },
			HeavyWeapons = { };

		public enum PedComponent
		{
			Face = 0,
			Head = 1,
			Hair = 2,
			Torso = 3,
			Legs = 4,
			Hands = 5,
			Feet = 6,
			Eyes = 7,
			Accessories = 8,
			Tasks = 9,
			Textures = 10,
			Torso2 = 11
		}

		public enum PedCombatRange
		{
			Near = 0,
			Medium = 1,
			Far = 2
		}

		public enum PedCombatAbility
		{
			Poor = 0,
			Average = 1,
			Professional = 2
		}

		private static TEnum[] ParseEnums<TEnum>(string[] lines) where TEnum : struct
		{
			List<TEnum> enumsList = new List<TEnum>();

			foreach (string line in lines)
			{
				string enumStr = line;

				int commentIndex = enumStr.IndexOf("//");
				if (commentIndex >= 0) enumStr = enumStr.Remove(commentIndex);
				enumStr = enumStr.Trim();

				if (string.IsNullOrWhiteSpace(enumStr)) continue;

				TEnum parsedEnum;

				if (Enum.TryParse(enumStr, out parsedEnum))
				{
					enumsList.Add(parsedEnum);
				}
			}

			return enumsList.ToArray();
		}
		
		internal static void GetEnumLists()
		{
			//ped models
			string[] lines = File.ReadAllLines(pedModelsPath);
			PedModels = ParseEnums<PedHash>(lines);

			//knives
			lines = File.ReadAllLines(knifeWeaponsPath);
			KnifeWeapons = ParseEnums<WeaponHash>(lines);

			//melee
			lines = File.ReadAllLines(meleeWeaponsPath);
			MeleeWeapons = ParseEnums<WeaponHash>(lines);

			//pistols
			lines = File.ReadAllLines(pistolWeaponsPath);
			PistolWeapons = ParseEnums<WeaponHash>(lines);

			//smgs
			lines = File.ReadAllLines(smgWeaponsPath);
			SMGWeapons = ParseEnums<WeaponHash>(lines);

			//ARs
			lines = File.ReadAllLines(arWeaponsPath);
			ARWeapons = ParseEnums<WeaponHash>(lines);

			//MGs
			lines = File.ReadAllLines(mgWeaponsPath);
			MGWeapons = ParseEnums<WeaponHash>(lines);

			//shotguns
			lines = File.ReadAllLines(shotgunWeaponsPath);
			ShotgunWeapons = ParseEnums<WeaponHash>(lines);

			//snipers
			lines = File.ReadAllLines(sniperWeaponsPath);
			SniperWeapons = ParseEnums<WeaponHash>(lines);

			//heavies
			lines = File.ReadAllLines(heavyWeaponsPath);
			HeavyWeapons = ParseEnums<WeaponHash>(lines);
		}
	}
}

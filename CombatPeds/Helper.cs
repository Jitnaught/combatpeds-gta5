﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Collections.Generic;
using System.IO;
using static SpawnCombatPeds.Enums;

namespace SpawnCombatPeds
{
	static class Helper
	{
		private const string LOG_FILE_PATH = "./scripts/SpawnCombatPeds.log";

		public static void DeleteOldLog()
		{
			if (File.Exists(LOG_FILE_PATH)) File.Delete(LOG_FILE_PATH);
		}

		public static void Log(string text)
		{
			string timestamp = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff") + "]";

			File.AppendAllText(LOG_FILE_PATH, timestamp + text + "\r\n");
		}

		public static WeaponHash[] GenerateWeaponsList()
		{
			List<WeaponHash> weaponHashes = new List<WeaponHash>();

			if (INI.UseFistWeapon) weaponHashes.Add(WeaponHash.Unarmed);
			if (INI.UseKnifeWeapons) weaponHashes.AddRange(KnifeWeapons);
			if (INI.UseMeleeWeapons) weaponHashes.AddRange(MeleeWeapons);
			if (INI.UsePistolWeapons) weaponHashes.AddRange(PistolWeapons);
			if (INI.UseTaserWeapon) weaponHashes.Add(WeaponHash.StunGun);
			if (INI.UseSMGWeapons) weaponHashes.AddRange(SMGWeapons);
			if (INI.UseAssaultRifleWeapons) weaponHashes.AddRange(ARWeapons);
			if (INI.UseMGWeapons) weaponHashes.AddRange(MGWeapons);
			if (INI.UseShotgunWeapons) weaponHashes.AddRange(ShotgunWeapons);
			if (INI.UseSniperWeapons) weaponHashes.AddRange(SniperWeapons);
			if (INI.UseHeavyWeapons) weaponHashes.AddRange(HeavyWeapons);

			return weaponHashes.ToArray();
		}

		public static bool IsSeatFreeOrPedDead(Vehicle veh, VehicleSeat seat)
		{
			if (veh.IsSeatFree(seat)) return true;

			Ped ped = veh.GetPedOnSeat(seat);

			if (ped == null || !ped.Exists() || ped.IsDead) return true;

			return false;
		}

		public static VehicleSeat GetEmptyVehicleSeat(Vehicle veh)
		{
			if (IsSeatFreeOrPedDead(veh, VehicleSeat.Driver)) return VehicleSeat.Driver;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.Passenger)) return VehicleSeat.Passenger;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.LeftRear)) return VehicleSeat.LeftRear;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.RightRear)) return VehicleSeat.RightRear;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat1)) return VehicleSeat.ExtraSeat1;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat2)) return VehicleSeat.ExtraSeat2;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat3)) return VehicleSeat.ExtraSeat3;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat4)) return VehicleSeat.ExtraSeat4;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat5)) return VehicleSeat.ExtraSeat5;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat6)) return VehicleSeat.ExtraSeat6;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat7)) return VehicleSeat.ExtraSeat7;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat8)) return VehicleSeat.ExtraSeat8;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat9)) return VehicleSeat.ExtraSeat9;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat10)) return VehicleSeat.ExtraSeat10;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat11)) return VehicleSeat.ExtraSeat11;
			else if (IsSeatFreeOrPedDead(veh, VehicleSeat.ExtraSeat12)) return VehicleSeat.ExtraSeat12;

			return VehicleSeat.Any;
		}

		public static void ForcePedInteriorFromPed(Ped fromPed, Ped toPed)
		{
			int interior = Function.Call<int>(Hash.GET_INTERIOR_FROM_ENTITY, fromPed);
			int roomKey = Function.Call<int>(Hash.GET_KEY_FOR_ENTITY_IN_ROOM, fromPed);
			Function.Call(Hash.FORCE_ROOM_FOR_ENTITY, toPed, interior, roomKey);
		}

		public static void SetPosition(Ped ped, Vector3 position, bool clearArea = false)
		{
			//set position manually so area isn't cleared of entities
			Function.Call(Hash.SET_ENTITY_COORDS, ped, position.X, position.Y, position.Z, false, false, false, clearArea); //last parameter = clearArea
		}

		public static Vehicle GetVehicle(Ped ped)
		{
			Vehicle veh = null;

			if (ped.IsInVehicle())
			{
				veh = ped.CurrentVehicle;

				if (veh == null || !veh.Exists() || veh.IsDead) veh = null;
			}

			return veh;
		}

		public static void FixPedClothes(Ped ped, PedHash model)
		{
			//SET_PED_COMPONENT_VARIATION(Ped ped, int componentId, int drawableId, int textureId, int paletteId)

			switch (model)
			{
				case PedHash.JohnnyKlebitz:
				case PedHash.Lost01GFY:
				case PedHash.Salton04AMM:
					Function.Call(Hash.SET_PED_COMPONENT_VARIATION, ped, (int)PedComponent.Face, 1, 0, 0);
					break;
				case PedHash.Casino01SMY:
				case PedHash.WestSec01SMY:
					Function.Call(Hash.SET_PED_COMPONENT_VARIATION, ped, (int)PedComponent.Torso, 1, 0, 0);
					break;
				case PedHash.LamarDavis:
					Function.Call(Hash.SET_PED_COMPONENT_VARIATION, ped, (int)PedComponent.Hands, 2, 0, 0);
					break;
				case PedHash.Casino01SFY:
					Function.Call(Hash.SET_PED_COMPONENT_VARIATION, ped, (int)PedComponent.Torso, 1, 0, 0);
					Function.Call(Hash.SET_PED_COMPONENT_VARIATION, ped, (int)PedComponent.Legs, 1, 0, 0);
					break;
				case PedHash.Hippie01AFY:
				case PedHash.Baywatch01SMY:
					Function.Call(Hash.SET_PED_COMPONENT_VARIATION, ped, (int)PedComponent.Textures, 1, 0, 0);
					break;
				case PedHash.TaylorFemaleYoung:
				case PedHash.BennyMech01F:
					Function.Call(Hash.SET_PED_PROP_INDEX, ped, 0, 0, 0, true);
					break;
			}
		}

		public static Blip AddPedBlip(Ped ped)
		{
			Blip blip = ped.AddBlip();
			blip.Scale = 0.7f;
			blip.IsFriendly = true;

			return blip;
		}
	}
}

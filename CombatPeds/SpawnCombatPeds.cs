﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GTA;
using GTA.Native;
using GTA.Math;
using static SpawnCombatPeds.Enums;

namespace SpawnCombatPeds
{
	public class SpawnCombatPeds : Script
	{
		private const PedCombatRange COMBAT_RANGE_LOWEST = PedCombatRange.Near, COMBAT_RANGE_HIGHEST = PedCombatRange.Far;
		private const PedCombatAbility COMBAT_ABILITY_LOWEST = PedCombatAbility.Poor, COMBAT_ABILITY_HIGHEST = PedCombatAbility.Professional;
		private const int COMBAT_ACCURACY_LOWEST = 10, COMBAT_ACCURACY_HIGHEST = 100;

		private List<CombatPed> combatants = new List<CombatPed>();
		private List<Ped> deadCombatants = new List<Ped>();
		private Random rand = new Random();
		private bool wereMultipleAliveCombatants = false;
		private double lastTimeRanCombatTick = 0;

		public SpawnCombatPeds()
		{
			Helper.DeleteOldLog();
			INI.GetSettings(Settings);
			GetEnumLists();

			Interval = 0;
			Tick += OnTick;
			KeyDown += OnKeyDown;
			Aborted += OnAbort;
		}

		private Vector3 GetPlayerOrCamPos()
		{
			Camera renderCam = World.RenderingCamera;

			if (renderCam == null || !renderCam.Exists()) return Game.Player.Character.Position;

			return renderCam.Position;
		}

		private Ped GetClosestCombatant(Ped ped)
		{
			float closestDistance = float.MaxValue;
			Ped closestPed = null;

			foreach (var combatant in combatants)
			{
				Ped combatPed = combatant.ped;

				if (ped != combatPed)
				{
					float distance = ped.Position.DistanceTo(combatPed.Position);

					if (distance < closestDistance)
					{
						closestDistance = distance;
						closestPed = combatPed;
					}
				}

				Yield();
			}

			return closestPed;
		}

		private CombatPed GetRandomCombatant(int ignoreIndex)
		{
			int randNum = rand.Next(0, combatants.Count);

			if (randNum == ignoreIndex)
			{
				if (randNum - 1 < 0) randNum++;
				else randNum--;
			}

			return combatants[randNum];
		}

		private void GivePedRandomWeapon(Ped ped)
		{
			WeaponHash[] mainWeaponHashes = Helper.GenerateWeaponsList();

			if (mainWeaponHashes.Length > 0)
			{
				//secondary knife
				if (INI.UseSecondaryKnifeWeapon && KnifeWeapons.Length > 0)
				{
					WeaponHash weaponHash = KnifeWeapons[rand.Next(KnifeWeapons.Length)];
					ped.Weapons.Give(weaponHash, 1, true, true);
				}

				//secondary pistol (hopefully fix final stand)
				if (PistolWeapons.Length > 0)
				{
					WeaponHash weaponHash = PistolWeapons[rand.Next(PistolWeapons.Length)];
					ped.Weapons.Give(weaponHash, 9999, true, true);
				}

				//main weapon
				WeaponHash mainWeaponHash = mainWeaponHashes[0];
				if (mainWeaponHashes.Length > 1) mainWeaponHash = mainWeaponHashes[rand.Next(mainWeaponHashes.Length)];

				ped.Weapons.Give(mainWeaponHash, 9999, true, true);

				if (mainWeaponHash == WeaponHash.Minigun || mainWeaponHash == WeaponHash.Widowmaker)
				{
					ped.FiringPattern = FiringPattern.FullAuto;
				}
			}
		}

		private void SetCombatantRelationships(Ped ped)
		{
			//make ped companion with player, so hopefully won't shoot him
			ped.IsEnemy = false;
			int plrRelationshipGroup = Game.Player.Character.RelationshipGroup, pedRelGroup = ped.RelationshipGroup;
			World.SetRelationshipBetweenGroups(Relationship.Companion, pedRelGroup, plrRelationshipGroup);
			World.SetRelationshipBetweenGroups(Relationship.Companion, plrRelationshipGroup, pedRelGroup);

			//make ped hate cops
			World.SetRelationshipBetweenGroups(Relationship.Hate, pedRelGroup, Game.GenerateHash("COP"));

			if (combatants.Count > 0)
			{
				//make ped hate all other combatants, to hopefully spark some combat

				foreach (var combatant in combatants)
				{
					Ped combatantPed = combatant.ped;
					int combatantPedRelGroup = combatantPed.RelationshipGroup;

					World.SetRelationshipBetweenGroups(Relationship.Hate, pedRelGroup, combatantPedRelGroup);
					World.SetRelationshipBetweenGroups(Relationship.Hate, combatantPedRelGroup, pedRelGroup);

					Yield();
				}
			}

			//ped.NeverLeavesGroup = true;
			//plrPed.CurrentPedGroup.Add(ped, false);
		}

		private void SetCombatantCombatVariables(Ped ped)
		{
			int accuracy = 60, ability = (int)PedCombatAbility.Professional, range = (int)PedCombatRange.Far;

			//don't set COMBAT_MOVEMENT; it seems to break movement altogether

			if (INI.RandomizeAccuracy)
			{
				accuracy = rand.Next(COMBAT_ACCURACY_LOWEST, COMBAT_ACCURACY_HIGHEST + 1);
				Helper.Log("Random accuracy: " + accuracy.ToString());
			}

			ped.Accuracy = accuracy; //100 = perfect

			if (INI.RandomizeAbility)
			{
				ability = rand.Next((int)COMBAT_ABILITY_LOWEST, (int)COMBAT_ABILITY_HIGHEST + 1);
				Helper.Log("Random ability: " + ((PedCombatAbility)ability).ToString());
			}

			Function.Call(Hash.SET_PED_COMBAT_ABILITY, ped, ability);

			if (INI.RandomizeRange)
			{
				range = rand.Next((int)COMBAT_RANGE_LOWEST, (int)COMBAT_RANGE_HIGHEST + 1);
				Helper.Log("Random range: " + ((PedCombatRange)range).ToString());
			}

			Function.Call(Hash.SET_PED_COMBAT_RANGE, ped, range);

			ped.CanSufferCriticalHits = INI.SufferCriticalHits; //headshots instant kill or not
			ped.DropsWeaponsOnDeath = true;
			ped.DiesInstantlyInWater = false;
			ped.DrownsInWater = false;

			ped.CanSwitchWeapons = INI.CanSwitchWeapons;

			Function.Call(Hash.SET_PED_CAN_BE_TARGETED_WHEN_INJURED, ped, true);
			Function.Call(Hash.SET_PED_CAN_BE_TARGETED_WITHOUT_LOS, ped, true);

			Function.Call(Hash.SET_PED_MAX_TIME_IN_WATER, ped, 9999f);
			Function.Call(Hash.SET_PED_MAX_TIME_UNDERWATER, ped, 100f);

			Function.Call(Hash.SET_PED_PATHS_WIDTH_PLANT, ped, true); //SET_PED_PATH_MAY_ENTER_WATER
			Function.Call(Hash.SET_PED_PATH_PREFER_TO_AVOID_WATER, ped, false);
			Function.Call(Hash.SET_PED_PATH_CAN_DROP_FROM_HEIGHT, ped, true);
			Function.Call(Hash.SET_PED_PATH_CAN_USE_CLIMBOVERS, ped, true);
			//Function.Call(Hash.SET_PED_PATH_CAN_USE_LADDERS, ped, true);

			Function.Call(Hash._0x52D59AB61DDC05DD, ped, true); //SET_PED_HIGHLY_PERCEPTIVE
			Function.Call(Hash.SET_PED_SEEING_RANGE, ped, INI.SeeingRange); //range in meters

			Function.Call(Hash.SET_PED_CAN_COWER_IN_COVER, ped, false);
			
			//Function.Call(Hash.SET_PED_ALERTNESS, ped, 3); //ranges from 0 to 3

			//ped.ShootRate = 1000;
			//ped.DrownsInWater = false;

			Function.Call(Hash.SET_PED_COMBAT_ATTRIBUTES, ped, 1, true); //1 = CanUseVehicles
			Function.Call(Hash.SET_PED_COMBAT_ATTRIBUTES, ped, 2, true); //2 = CanDoDrivebys
			Function.Call(Hash.SET_PED_COMBAT_ATTRIBUTES, ped, 3, true); //3 = CanLeaveVehicle
			Function.Call(Hash.SET_PED_COMBAT_ATTRIBUTES, ped, 5, true); //5 = CanFightArmedPedsWhenNotArmed
			Function.Call(Hash.SET_PED_COMBAT_ATTRIBUTES, ped, 17, false); //17 = flee from peds with weapons?
			Function.Call(Hash.SET_PED_COMBAT_ATTRIBUTES, ped, 46, true); //46 = AlwaysFight

			//Function.Call(Hash.SET_COMBAT_FLOAT, ped, 0, 0.5f); //0 = BlindFireChance, default = 0.1
			//Function.Call(Hash.SET_COMBAT_FLOAT, ped, 1, 2.0f); //1 = BurstDurationInCover, default = 2.0
			//Function.Call(Hash.SET_COMBAT_FLOAT, ped, 3, 0.5f); //3 = TimeBetweenBurstsInCover, default = 1.25
			//Function.Call(Hash.SET_COMBAT_FLOAT, ped, 4, 1.5f); //4 = TimeBetweenPeeks, default = 10.0
			//Function.Call(Hash.SET_COMBAT_FLOAT, ped, 11, 10f); //11 = AttackWindowDistanceForCover, default = 55.0
			//Function.Call(Hash.SET_COMBAT_FLOAT, ped, 16, 1f); //16 = OptimalCoverDistance, default = 21.0?

			//Function.Call(Hash.SET_PED_FLEE_ATTRIBUTES, ped, 0, false);
		}

		private void TaskCombatantsToFight()
		{
			if (combatants.Count > 1)
			{
				for (int i = 0; i < combatants.Count; i++)
				{
					Ped combatPed = combatants[i].ped;

					Ped pedToCombat = null;

					if (INI.RandomTargeting && combatants.Count > 2)
					{
						pedToCombat = GetRandomCombatant(i).ped;
					}
					else
					{
						if (i + 1 < combatants.Count) pedToCombat = combatants[i + 1].ped;
						else if (i - 1 >= 0) pedToCombat = combatants[i - 1].ped;
					}

					if (pedToCombat != null)
					{
						combatPed.Task.FightAgainst(pedToCombat);
					}

					Yield();
				}

				UI.ShowSubtitle("Fight!", 500);
			}
		}

		private void TaskCloseCombatantsToEnterVeh()
		{
			if (combatants.Count > 0)
			{
				foreach (var combatant in combatants)
				{
					Ped combatPed = combatant.ped;

					if (!combatPed.IsInVehicle() && combatPed.Position.DistanceTo(GetPlayerOrCamPos()) < INI.EnterVeh_MaxDistancePlrToPed)
					{
						Vehicle veh = World.GetClosestVehicle(combatPed.Position, INI.EnterVeh_MaxDistancePedToVeh);

						if (veh != null && veh.Exists() && veh.IsAlive)
						{
							VehicleSeat emptySeat = Helper.GetEmptyVehicleSeat(veh);

							if (emptySeat != VehicleSeat.Any) //if found an empty seat
							{
								combatPed.Task.EnterVehicle(veh, emptySeat);
							}
						}
					}
				}

				UI.ShowSubtitle("Closest peds enter closest vehicle");
			}
		}

		private void HandleCombatantDeletions()
		{
			for (int i = 0; i < combatants.Count; i++)
			{
				var combatant = combatants[i];
				Ped ped = combatant.ped;
				Blip blip = combatant.blip;

				if (ped == null || !ped.Exists())
				{
					if (blip != null && blip.Exists()) blip.Remove();

					combatant.model.MarkAsNoLongerNeeded();

					combatants.RemoveAt(i);
					i--;
				}
				else if (ped.IsDead)
				{
					if (blip != null && blip.Exists()) blip.Remove();

					deadCombatants.Add(ped);

					combatants.RemoveAt(i);
					i--;
				}

				Yield();
			}

			for (int i = 0; i < deadCombatants.Count; i++)
			{
				if (deadCombatants[i] == null || !deadCombatants[i].Exists())
				{
					deadCombatants.RemoveAt(i);
					i--;
				}

				Yield();
			}

			if (deadCombatants.Count >= INI.MaxDeadCombatants)
			{
				Ped firstPed = deadCombatants[0];
				Model firstPedModel = firstPed.Model;

				Helper.Log("Marking dead " + firstPedModel.ToString() + " as no longer needed");

				firstPed.MarkAsNoLongerNeeded();
				firstPedModel.MarkAsNoLongerNeeded();
			}
		}

		private void HandleCombatantCombat()
		{
			if (combatants.Count > 1)
			{
				wereMultipleAliveCombatants = true;

				bool anyInCombat = false;

				foreach (var combatant in combatants)
				{
					Ped combatPed = combatant.ped;

					if (combatPed.IsInCombat || combatPed.IsInMeleeCombat || combatant.moveToNearestPed)
					{
						anyInCombat = true;
						break;
					}

					Yield();
				}

				if (anyInCombat)
				{
					for (int i = 0; i < combatants.Count; i++)
					{
						CombatPed combatant = combatants[i];
						Ped combatPed = combatant.ped;

						float closestDistance = float.MaxValue;

						Ped closestPed = null;
						bool seesAnyCombatant = false;

						foreach (var otherCombatant in combatants)
						{
							Ped otherCombatPed = otherCombatant.ped;

							if (combatPed != otherCombatPed)
							{
								float distance = combatPed.Position.DistanceTo(otherCombatPed.Position);

								if (distance < closestDistance)
								{
									closestDistance = distance;
									closestPed = otherCombatPed;
								}

								if (!seesAnyCombatant && Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, combatPed, otherCombatPed, 17))
								{
									seesAnyCombatant = true;
								}
							}

							Yield();
						}

						if (closestPed != null)
						{
							if (combatPed.IsSwimming || combatPed.IsSwimmingUnderWater || (closestDistance > 50f && !seesAnyCombatant) || closestDistance > 100f)
							{
								//go straight to closest combatant if too far away for pathing, or if in water

								if (combatPed.Position.DistanceTo(combatant.lastPosWhileRunning) < 0.3f)
								{
									if (combatant.timeFirstStuck == 0) combatant.timeFirstStuck = Game.GameTime;

									if (Game.GameTime - combatant.timeFirstStuck >= 2000) //ms stuck for
									{
										combatant.getUnstuck = true;
										combatant.timeStopUnstuck = Game.GameTime + 4000; //ms to try to get unstuck for
									}
								}
								else
								{
									combatant.timeFirstStuck = 0;
								}

								if (!combatant.getUnstuck || Game.GameTime >= combatant.timeStopUnstuck)
								{
									combatant.getUnstuck = false;
									combatant.runningToUnstuck = false;

									combatant.moveToNearestPed = true;

									Vehicle veh = Helper.GetVehicle(combatPed);

									if (veh == null)
									{
										combatPed.Task.RunTo(closestPed.Position, true);
									}
									else
									{
										combatPed.Task.DriveTo(veh, closestPed.Position, 25f, 30f, (int)DrivingStyle.Rushed);
									}

									combatants[i].lastPosWhileRunning = combatPed.Position;

									//UI.ShowSubtitle("GoTo closest ped");
								}
								else if (!combatant.runningToUnstuck)
								{
									Vector3 offsetCoord = combatPed.GetOffsetInWorldCoords(new Vector3(20f, -1f, 0f));

									Vehicle veh = Helper.GetVehicle(combatPed);

									if (veh == null)
									{
										combatPed.Task.RunTo(offsetCoord);
									}
									else
									{
										combatPed.Task.DriveTo(veh, offsetCoord, 50f, 30f, (int)DrivingStyle.Rushed);
									}

									combatant.runningToUnstuck = true;

									//UI.ShowSubtitle("GoTo unstuck");
								}
							}
							else if (closestDistance > 50f && !seesAnyCombatant)
							{
								//go to closest combatant if can't see any combatants

								combatant.moveToNearestPed = true;

								if (Function.Call<int>(Hash.GET_SCRIPT_TASK_STATUS, combatPed, Game.GenerateHash("SCRIPT_TASK_GOTO_ENTITY_AIMING")) == 7)
								{
									Function.Call(Hash.TASK_GOTO_ENTITY_AIMING, combatPed, closestPed, 1f, 5f);

									//UI.ShowSubtitle("GoToAiming closest ped");
								}
							}
							else if (!combatPed.IsInCombat)
							{
								//fight nearest combatant if not already fighting somebody

								combatant.moveToNearestPed = false;

								Ped pedToCombat = null;

								if (INI.RandomTargeting) pedToCombat = GetRandomCombatant(i).ped;
								else pedToCombat = closestPed;

								combatPed.Task.FightAgainst(pedToCombat);

								//UI.ShowSubtitle("Fight closest ped");
							}
							else combatant.moveToNearestPed = false;
						}

						Yield();
					}
				}
			}
			else if (combatants.Count == 1 && wereMultipleAliveCombatants)
			{
				wereMultipleAliveCombatants = false;

				Ped combatPed = combatants[0].ped;

				if (combatPed.IsInVehicle())
				{
					combatPed.Task.LeaveVehicle(LeaveVehicleFlags.None);

					while (combatPed.IsInVehicle()) Yield();

					combatPed.Task.StandStill(-1);
				}
				else combatPed.Task.StandStill(-1);
			}
		}

		private void DeleteOldCombatant()
		{
			if (combatants.Count >= INI.MaxCombatants)
			{
				var firstCombatant = combatants[0];
				Ped firstPed = firstCombatant.ped;
				Blip firstBlip = firstCombatant.blip;

				if (firstPed != null && firstPed.Exists())
				{
					Model firstPedModel = firstPed.Model;

					firstPed.Delete();
					firstPedModel.MarkAsNoLongerNeeded();
				}

				if (firstBlip != null && firstBlip.Exists())
				{
					firstBlip.Remove();
				}

				combatants.RemoveAt(0);
			}
		}

		private void OnAbort(object sender, EventArgs e)
		{
			foreach (var combatant in combatants)
			{
				Ped ped = combatant.ped;
				Blip blip = combatant.blip;

				if (ped != null && ped.Exists())
				{
					Model model = ped.Model;
					ped.MarkAsNoLongerNeeded();
					model.MarkAsNoLongerNeeded();
				}

				if (blip != null && blip.Exists())
				{
					blip.Remove();
				}
			}

			combatants.Clear();

			foreach (var deadCombatant in combatants)
			{
				Ped ped = deadCombatant.ped;

				if (ped != null && ped.Exists())
				{
					Model model = ped.Model;
					ped.Delete();
					model.MarkAsNoLongerNeeded();
				}
			}

			deadCombatants.Clear();
		}

		private void OnTick(object sender, EventArgs e)
		{
			if (DateTime.UtcNow.TimeOfDay.TotalMilliseconds >= lastTimeRanCombatTick + 50)
			{
				HandleCombatantDeletions();
				if (INI.CustomCombat) HandleCombatantCombat();

				lastTimeRanCombatTick = DateTime.UtcNow.TimeOfDay.TotalMilliseconds;
			}
		}

		private void OnKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == INI.SpawnKey)
			{
				DeleteOldCombatant();

				Ped plrPed = Game.Player.Character;

				Vector3 offset = new Vector3(0f, 3f, 0f);
				Vector3 spawnPos = Vector3.Zero;

				if (World.RenderingCamera == null || !World.RenderingCamera.Exists()) spawnPos = plrPed.GetOffsetInWorldCoords(offset);
				else spawnPos = World.RenderingCamera.GetOffsetInWorldCoords(offset);

				PedHash pedHash = PedModels[rand.Next(PedModels.Length)];

				UI.ShowSubtitle(pedHash.ToString(), 1500);
				Helper.Log("Spawned peds: " + combatants.Count.ToString() + ", new ped hash: " + pedHash.ToString());

				Ped ped = World.CreatePed(pedHash, Vector3.Zero);
				if (ped == null || !ped.Exists()) return;

				ped.SetDefaultClothes();
				Helper.FixPedClothes(ped, pedHash);
				GivePedRandomWeapon(ped);

				SetCombatantRelationships(ped);
				//ped.RelationshipGroup = Game.GenerateHash("AGGRESSIVE_INVESTIGATE");

				SetCombatantCombatVariables(ped);
				Helper.ForcePedInteriorFromPed(plrPed, ped);
				ped.Money = 0;

				Wait(200);

				ped.Task.ClearAllImmediately();
				Helper.SetPosition(ped, spawnPos);
				ped.Heading = (GetPlayerOrCamPos() - spawnPos).ToHeading();

				Blip blip = Helper.AddPedBlip(ped);

				ped.Task.StandStill(-1); //-1 = forever

				combatants.Add(new CombatPed(ped, blip));
			}
			else if (e.KeyCode == INI.FightKey)
			{
				TaskCombatantsToFight();
			}
			else if (e.KeyCode == INI.EnterVehKey)
			{
				TaskCloseCombatantsToEnterVeh();
			}
			else if (e.KeyCode == INI.ToggleMenuKey)
			{
				Menu.Toggle();
			}
		}
	}
}
